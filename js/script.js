function showAllGas() {
  $.getJSON("data/gas.json", function (data) {
    let jenis = data.jenis;
    $("#jenis-gas").empty();
    $.each(jenis, function (i, data) {
      $("#jenis-gas").append(
        '<div class="col-md-4"><div class="card mb-3"><img src="img/gas/' +
          data.gambar +
          '" class="card-img-top card-image"><div class="card-body"><h4 class="card-title text-center">' +
          data.nama +
          '</h4><p class="card-text" style="text-align: justify;">' +
          data.deskripsi +
          '</p><table border="0" cellpadding="5"><tbody><tr><td style="padding-bottom: 0"><h6 class="card-title">Harga tabung + isi</h6></td><td style="padding-bottom: 0"><h6 class="card-title">=</h6></td><td style="padding-bottom: 0"><h6 class="card-title">Rp ' +
          data.bruto +
          '</h6></td></tr><tr><td style="padding-top: 0"><h6 class="card-title">Harga isi / refill</h6></td><td style="padding-top: 0"><h6 class="card-title">=</h6></td><td style="padding-top: 0"><h6 class="card-title">Rp ' +
          data.netto +
          '</h6></td></tr></h6></tbody></table><div class="text-center" style="margin-top:10px"><a href="https://wa.link/d2gypu" target="_blank" class="btn btn-primary">Beli Sekarang</a></div></div></div></div>'
      );
    });
  });
}

// ALL GASES
showAllGas();

$(".ktg").on("click", function () {
  $(".ktg").removeClass("active");
  $(this).addClass("active");

  let kategori = $(this).html();
  $("h1").html(kategori);

  if (kategori == "All Gas") {
    showAllGas();
    return;
  }

  $.getJSON("data/gas.json", function (data) {
    let jenis = data.jenis;
    let content = "";

    $.each(jenis, function (i, data) {
      if (data.kategori == kategori.toLowerCase()) {
        content +=
          '<div class="col-md-4"><div class="card mb-3"><img src="img/gas/' +
          data.gambar +
          '" class="card-img-top card-image"><div class="card-body"><h4 class="card-title text-center">' +
          data.nama +
          '</h4><p class="card-text" style="text-align: justify;">' +
          data.deskripsi +
          '</p><table border="0" cellpadding="5"><tbody><tr><td style="padding-bottom: 0"><h6 class="card-title">Harga tabung + isi</h6></td><td style="padding-bottom: 0"><h6 class="card-title">=</h6></td><td style="padding-bottom: 0"><h6 class="card-title">Rp ' +
          data.bruto +
          '</h6></td></tr><tr><td style="padding-top: 0"><h6 class="card-title">Harga isi / refill</h6></td><td style="padding-top: 0"><h6 class="card-title">=</h6></td><td style="padding-top: 0"><h6 class="card-title">Rp ' +
          data.netto +
          '</h6></td></tr></h6></tbody></table><div class="text-center" style="margin-top:10px"><a href="https://wa.link/d2gypu" target="_blank" class="btn btn-primary">Beli Sekarang</a></div></div></div></div>';
      }
    });
    $("#jenis-gas").html(content);
  });
});
